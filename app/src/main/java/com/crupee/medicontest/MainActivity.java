package com.crupee.medicontest;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;


import com.crupee.medicontest.adapter.CustomAdapter;
/*import com.robotemi.sdk.Robot;
import com.robotemi.sdk.TtsRequest;
import com.robotemi.sdk.listeners.OnGoToLocationStatusChangedListener;
import com.robotemi.sdk.listeners.OnLocationsUpdatedListener;
import com.robotemi.sdk.listeners.OnRobotReadyListener;

import org.jetbrains.annotations.NotNull;*/

import java.util.List;

import static com.crupee.medicontest.utility.UtilityClass.hideKeyboard;

public class MainActivity extends AppCompatActivity /*implements
        OnRobotReadyListener,
        Robot.TtsListener,
        OnGoToLocationStatusChangedListener,
        OnLocationsUpdatedListener */ {

    //views
    EditText etSaveLocation, etGoTo, etSpeak;
    Button btnSaveLocation, btnGoTo, btnSpeak, btnSavedLocations;
    SeekBar seekBarBrightness, seekBarMusicVolume, seekBarAlarmVolume, seekBarRingVolume, seekBarSystemVolume, seekBarVoiceVolume;
    TextView txtPercentage, txtMusicVolumePercentage, txtAlarmVolumePercentage, txtRingVolumePercentage, txtSystemVolumePercentage, txtVoiceVolumePercentage;

    //instance
    //private Robot robot;

    //variables
    List<String> locations;

    //Variable to store brightness value
    private int brightness;
    //Content resolver used as a handle to the system's settings
    private ContentResolver cResolver;
    //Window object, that will store a reference to the current window
    private Window window;


    MediaPlayer mediaPlayer;
    AudioManager audioManager;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();

        clickMethod();


        if (Settings.System.canWrite(MainActivity.this)) {
            // Do stuff here
            setBrightnessSeekBar();
        } else {
            Intent intent = new Intent(android.provider.Settings.ACTION_MANAGE_WRITE_SETTINGS);
            intent.setData(Uri.parse("package:" + getPackageName()));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }

        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        setVolumeSeekBar(seekBarMusicVolume, AudioManager.STREAM_MUSIC);
        setVolumeSeekBar(seekBarAlarmVolume, AudioManager.STREAM_ALARM);
        setVolumeSeekBar(seekBarRingVolume, AudioManager.STREAM_RING);
        setVolumeSeekBar(seekBarSystemVolume, AudioManager.STREAM_SYSTEM);
        setVolumeSeekBar(seekBarVoiceVolume, AudioManager.STREAM_VOICE_CALL);
    }


    private void initView() {

        //robot = Robot.getInstance();

        etSaveLocation = (EditText) findViewById(R.id.etSaveLocation);
        etGoTo = (EditText) findViewById(R.id.etGoTo);
        etSpeak = (EditText) findViewById(R.id.etSpeak);
        btnSaveLocation = (Button) findViewById(R.id.btnSaveLocation);
        btnGoTo = (Button) findViewById(R.id.btnGoTo);
        btnSpeak = (Button) findViewById(R.id.btnSpeak);
        btnSavedLocations = (Button) findViewById(R.id.btnSavedLocations);

        seekBarBrightness = (SeekBar) findViewById(R.id.seekBarBrightness);
        txtPercentage = (TextView) findViewById(R.id.txtPercentage);

        seekBarMusicVolume = (SeekBar) findViewById(R.id.seekBarMusicVolume);
        txtMusicVolumePercentage = (TextView) findViewById(R.id.txtMusicVolumePercentage);

        seekBarAlarmVolume = (SeekBar) findViewById(R.id.seekBarAlarmVolume);
        txtAlarmVolumePercentage = (TextView) findViewById(R.id.txtAlarmVolumePercentage);

        seekBarRingVolume = (SeekBar) findViewById(R.id.seekBarRingVolume);
        txtRingVolumePercentage = (TextView) findViewById(R.id.txtRingVolumePercentage);

        seekBarSystemVolume = (SeekBar) findViewById(R.id.seekBarSystemVolume);
        txtSystemVolumePercentage = (TextView) findViewById(R.id.txtSystemVolumePercentage);

        seekBarVoiceVolume = (SeekBar) findViewById(R.id.seekBarVoiceVolume);
        txtVoiceVolumePercentage = (TextView) findViewById(R.id.txtVoiceVolumePercentage);
    }


    private void clickMethod() {
        btnSaveLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //saveLocation(view);
            }
        });

        btnGoTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //goTo(view);
            }
        });

        btnSpeak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //speak(view);
            }
        });

        btnSavedLocations.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //savedLocationsDialog(view);
            }
        });


    }


    private void setBrightnessSeekBar() {

        //Get the content resolver
        cResolver = getContentResolver();

        //Get the current window
        window = getWindow();

        //Set the seekbar range between 0 and 255
        //seek bar settings//
        //sets the range between 0 and 255
        seekBarBrightness.setMax(255);
        //set the seek bar progress to 1
        seekBarBrightness.setKeyProgressIncrement(1);

        brightness = Settings.System.getInt(cResolver, Settings.System.SCREEN_BRIGHTNESS, 0);
        /*try {
            //Get the current system brightness

        } catch (Settings.SettingNotFoundException e) {
            //Throw an error case it couldn't be retrieved
            Log.e("Error", "Cannot access system brightness");
            e.printStackTrace();
        }*/

        //Set the progress of the seek bar based on the system's brightness
        seekBarBrightness.setProgress(brightness);

        //Register OnSeekBarChangeListener, so it can actually change values
        seekBarBrightness.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onStopTrackingTouch(SeekBar seekBar) {
                //Set the system brightness using the brightness variable value
                Settings.System.putInt(cResolver, Settings.System.SCREEN_BRIGHTNESS, brightness);
                //Get the current window attributes
                WindowManager.LayoutParams layoutpars = window.getAttributes();
                //Set the brightness of this window
                layoutpars.screenBrightness = brightness / (float) 255;
                //Apply attribute changes to this window
                window.setAttributes(layoutpars);
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                //Nothing handled here
            }

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                //Set the minimal brightness level
                //if seek bar is 20 or any value below
                if (progress <= 20) {
                    //Set the brightness to 20
                    brightness = 20;
                } else //brightness is greater than 20
                {
                    //Set brightness variable based on the progress bar
                    brightness = progress;
                }
                //Calculate the brightness percentage
                float perc = (brightness / (float) 255) * 100;
                //Set the brightness percentage
                txtPercentage.setText((int) perc + " %");
            }
        });
    }


    private void setVolumeSeekBar(SeekBar bar, final int stream) {
        bar.setMax(audioManager.getStreamMaxVolume(stream));
        bar.setProgress(audioManager.getStreamVolume(stream));

        bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar bar, int progress,
                                          boolean fromUser) {
                audioManager.setStreamVolume(stream, progress,
                        AudioManager.FLAG_PLAY_SOUND);
            }

            public void onStartTrackingTouch(SeekBar bar) {
                // no-op
            }

            public void onStopTrackingTouch(SeekBar bar) {
                // no-op
            }
        });
    }

    /**
     * This is an example of saving locations.
     */
  /*  public void saveLocation(View view) {
        String location = etSaveLocation.getText().toString().toLowerCase().trim();
        boolean result = robot.saveLocation(location);
        if (result) {
            robot.speak(TtsRequest.create("I've successfully saved the " + location + " location.", true));
        } else {
            robot.speak(TtsRequest.create("Saved the " + location + " location failed.", true));
        }
        hideKeyboard(MainActivity.this);
    }*/

    /**
     * goTo checks that the location sent is saved then goes to that location.
     */
    /*public void goTo(View view) {
        for (String location : robot.getLocations()) {
            if (location.equals(etGoTo.getText().toString().toLowerCase().trim())) {
                robot.goTo(etGoTo.getText().toString().toLowerCase().trim());
                hideKeyboard(MainActivity.this);
            }
        }
    }*/

    /**
     * Have the robot speak while displaying what is being said.
     */
    /*public void speak(View view) {
        TtsRequest ttsRequest = TtsRequest.create(etSpeak.getText().toString().trim(), true);
        robot.speak(ttsRequest);
        hideKeyboard(MainActivity.this);
    }*/


    /**
     * Display the saved locations in a dialog
     */
   /* public void savedLocationsDialog(View view) {
        hideKeyboard(MainActivity.this);
        locations = robot.getLocations();
        final CustomAdapter customAdapter = new CustomAdapter(MainActivity.this, android.R.layout.simple_selectable_list_item, locations);
        AlertDialog.Builder versionsDialog = new AlertDialog.Builder(MainActivity.this);
        versionsDialog.setTitle("Saved Locations: (Click to delete the location)");
        versionsDialog.setPositiveButton("OK", null);
        versionsDialog.setAdapter(customAdapter, null);
        AlertDialog dialog = versionsDialog.create();
        dialog.getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                builder.setMessage("Delete location \"" + customAdapter.getItem(position) + "\" ?");
                builder.setPositiveButton("No thanks", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String location = customAdapter.getItem(position);
                        if (location == null) {
                            return;
                        }
                        boolean result = robot.deleteLocation(location);
                        if (result) {
                            locations.remove(position);
                            robot.speak(TtsRequest.create(location + "delete successfully!", false));
                            customAdapter.notifyDataSetChanged();
                        } else {
                            robot.speak(TtsRequest.create(location + "delete failed!", false));
                        }
                    }
                });
                Dialog deleteDialog = builder.create();
                deleteDialog.show();
            }
        });
        dialog.show();
    }*/

    /*@Override
    protected void onStart() {
        super.onStart();
        robot.addOnRobotReadyListener(this);
        robot.addTtsListener(this);
        robot.addOnGoToLocationStatusChangedListener(this);
        robot.addOnLocationsUpdatedListener(this);

    }

    @Override
    protected void onStop() {
        super.onStop();
        robot.removeOnRobotReadyListener(this);
        robot.removeTtsListener(this);
        robot.removeOnGoToLocationStatusChangedListener(this);
        robot.removeOnLocationsUpdateListener(this);
    }*/

    /*@Override
    public void onRobotReady(boolean isReady) {
        if (isReady) {
            try {
                final ActivityInfo activityInfo = getPackageManager().getActivityInfo(getComponentName(), PackageManager.GET_META_DATA);
                // Robot.getInstance().onStart() method may change the visibility of top bar.
                robot.onStart(activityInfo);
            } catch (PackageManager.NameNotFoundException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public void onTtsStatusChanged(@NotNull TtsRequest ttsRequest) {
        // Do whatever you like upon the status changing. after the robot finishes speaking
    }

    @Override
    public void onGoToLocationStatusChanged(@NotNull String location, String status, int descriptionId, @NotNull String description) {
        Log.d("GoToStatusChanged", "status=" + status + ", descriptionId=" + descriptionId + ", description=" + description);

        robot.speak(TtsRequest.create(description, false));
        switch (status) {
            case "start":
                robot.speak(TtsRequest.create("Starting", false));
                break;

            case "calculating":
                robot.speak(TtsRequest.create("Calculating", false));
                break;

            case "going":
                robot.speak(TtsRequest.create("Going", false));
                break;

            case "complete":
                robot.speak(TtsRequest.create("Completed", false));
                break;

            case "abort":
                robot.speak(TtsRequest.create("Cancelled", false));
                break;
        }
    }

    @Override
    public void onLocationsUpdated(@NotNull List<String> locations) {
        Toast.makeText(this, "Locations updated :\n" + locations, Toast.LENGTH_LONG).show();
    }*/
}
